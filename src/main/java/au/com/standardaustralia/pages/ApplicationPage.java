package au.com.standardaustralia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ApplicationPage {

	private WebDriver driver;

	public ApplicationPage(WebDriver driver) {
		this.driver = driver;
	}

	public void clickSingleButton() {

		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("singleSubmitButton"))));
		System.out.println("Click Single button");
		driver.findElement(By.id("singleSubmitButton")).click();

	}

	public void clickNoConcessionCardButton() throws InterruptedException {

		Thread.sleep(3000);
		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("btnHasConcessionCardNo"))));
		System.out.println("Click No button in concession cards section");
		driver.findElement(By.id("btnHasConcessionCardNo")).click();

	}

	public void selectTitle(String title) {

		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("Title"))));
		System.out.println("Select title");
		Select drpTitle = new Select(driver.findElement(By.id("Title")));
		drpTitle.selectByValue(title);

	}

	public void enterFirstName(String string) {

		System.out.println("Enter First Name");
		driver.findElement(By.id("FirstName")).sendKeys("John");

	}

	public void enterMiddleName(String string) {

		System.out.println("Enter Middle Name");
		driver.findElement(By.id("MiddleNames")).sendKeys("Dane");

	}

	public void enterLastName(String string) {

		System.out.println("Enter Last Name");
		driver.findElement(By.id("LastName")).sendKeys("Doe");

	}

	public void clickOK() {

		System.out.println("Click OK button");
		driver.findElement(By.id("nameSubmitButton")).click();

	}

	public boolean isEmailFieldDisplayed() {

		boolean flag = false;
		if (driver.findElement(By.cssSelector("[for=email]")).isDisplayed()) {
			flag = true;
			return flag;
		}
		return flag;
	}
}
