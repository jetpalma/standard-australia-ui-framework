package au.com.standardaustralia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EveryDaysAccountsPage {

	private WebDriver driver;

	public EveryDaysAccountsPage(WebDriver driver) {
		this.driver = driver;
	}

	public void clickOpenNow() {
		
		System.out.println("Click Open now button");
		driver.findElement(By.cssSelector("[title='Open now']")).click();
		
	}

	public void clickNewCustomerOpenNow() {

		System.out.println("Click Open now button for new customer");
		driver.findElement(By.cssSelector("[data-tracker-locationid='btn-open-now']")).click();
		
	}

	public ReadyToApplyPage clickGetStarted() {

		System.out.println("Click Get Started button");
		driver.findElement(By.cssSelector("[data-tracker-locationid='Btn_GetStarted']")).click();
		return new ReadyToApplyPage(driver);
	}

}
