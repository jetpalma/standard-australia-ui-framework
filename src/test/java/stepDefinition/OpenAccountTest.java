package stepDefinition;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import au.com.standardaustralia.pages.ApplicationPage;
import au.com.standardaustralia.pages.BankAccountsPage;
import au.com.standardaustralia.pages.EveryDaysAccountsPage;
import au.com.standardaustralia.pages.HomePage;
import au.com.standardaustralia.pages.ReadyToApplyPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class OpenAccountTest {

	WebDriver driver;
	BankAccountsPage bankAccountsPage;
	EveryDaysAccountsPage everyDaysAccountPage;
	ReadyToApplyPage readyToApplyPage;
	ApplicationPage applicationPage;

	@Given("that I'm in Commbank website")
	public void that_I_m_in_Commbank_website() {
		System.setProperty("webdriver.chrome.driver", "seleniumwebdriver/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.commbank.com.au/");

	}

	@When("I want to open an account")
	public void i_want_to_open_an_account() {

		HomePage homePage = new HomePage(driver);
		bankAccountsPage = homePage.clickBankAccounts();

	}

	@When("navigate to the process")
	public void navigate_to_the_process() throws InterruptedException {

		everyDaysAccountPage = bankAccountsPage.clickSeeAll();
		everyDaysAccountPage.clickOpenNow();
		everyDaysAccountPage.clickNewCustomerOpenNow();
		readyToApplyPage = everyDaysAccountPage.clickGetStarted();
		applicationPage = readyToApplyPage.clickOk();
		applicationPage.clickSingleButton();
		applicationPage.clickNoConcessionCardButton();

	}

	@When("provide the details about myself")
	public void provide_the_details_about_myself() {

		applicationPage.selectTitle("Mr");
		applicationPage.enterFirstName("John");
		applicationPage.enterMiddleName("Dane");
		applicationPage.enterLastName("Dane");
		applicationPage.clickOK();

	}

	@Then("section where email address and mobile will be shown")
	public void section_where_email_address_and_mobile_will_be_shown() {
		
		assertTrue(applicationPage.isEmailFieldDisplayed());


	}

}
