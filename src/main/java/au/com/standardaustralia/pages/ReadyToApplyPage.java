package au.com.standardaustralia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ReadyToApplyPage {

	private WebDriver driver;

	public ReadyToApplyPage(WebDriver driver) {
		this.driver = driver;
	}

	public ApplicationPage clickOk() {

		System.out.println("Click OK button");
		driver.findElement(By.id("landingSubmitButton")).click();
		return new ApplicationPage(driver);
		
	}

}
