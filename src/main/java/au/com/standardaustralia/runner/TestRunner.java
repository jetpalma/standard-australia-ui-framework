package au.com.standardaustralia.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/features/", glue= {"stepDefinition"}, plugin = { "pretty",
"html:target/cucumber" })
public class TestRunner {
}