package au.com.standardaustralia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

	private WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public BankAccountsPage clickBankAccounts() {
		
		System.out.println("Click Bank Accounts");
		driver.findElement(By.xpath("//*[text() = 'Bank accounts']")).click();
		return new BankAccountsPage(driver);
	}

}
