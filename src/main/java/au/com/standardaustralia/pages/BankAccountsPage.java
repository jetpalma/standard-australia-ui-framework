package au.com.standardaustralia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BankAccountsPage {

	private WebDriver driver;

	public BankAccountsPage(WebDriver driver) {
		this.driver = driver;
	}

	public EveryDaysAccountsPage clickSeeAll() {

		System.out.println("Click See All button");
		driver.findElement(By.cssSelector("[href*='everyday-accounts.html']")).click();	
		return new EveryDaysAccountsPage(driver);
	}

}
